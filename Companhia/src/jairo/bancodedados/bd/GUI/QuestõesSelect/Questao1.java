/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.GUI.QuestõesSelect;

import jairo.bancodedados.bd.GUI.pesquisador.*;
import jairo.bancodedados.bd.DAO.pesquisador.CadastroPesquisadorDAO;
import jairo.bancodedados.bd.telas.LoginUser;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jairochaves
 */
public class Questao1 extends javax.swing.JFrame {
    private JTable tabelaPesq;
    private JTable tabela2;
    private DefaultTableModel model=new DefaultTableModel();
    private DefaultTableModel model1=new DefaultTableModel();
    private CadastroPesquisadorDAO pesqDAO=new CadastroPesquisadorDAO();
    private ResultSet rs;
    
    
    /**
     * Creates new form ViewPesquisador
     */
    public Questao1() {
        
        initComponents();
        this.setLocationRelativeTo(null);
        inseretabela();
        
    }
    private void inseretabela(){
        
        tabelaPesq=new JTable(model);//1
        model.addColumn("Nome Projeto");//2
        tabelaPesq.getColumnModel().getColumn(0).setPreferredWidth(400);
        painelPesq.setViewportView(tabelaPesq);
        
        tabela2=new JTable(model1);
        model1.addColumn("Nome Pesq");
        model1.addColumn("Horas Semanais");
        
        painelPesq1.setViewportView(tabela2);
        
        
    }
    private void preencheTable(ResultSet sql,int questao){
        
        
        
        try {
            while (sql.next()) {
                if (questao==1) {
                    model.addRow(new String []{sql.getString("nome_projeto")});
                }else if(questao==2){
                    resNumProj.setText(sql.getInt(1)+"");
                }
                else if(questao==3){
                    model1.addRow(new String []{sql.getString("nome_pesquisador"),sql.getString("hora_semanal_pesquisador")});
                }else if(questao==4){
                    nome_Ger.setText(sql.getString("nome_pesquisador"));
                    Date data;
                    data = sql.getDate("data_nasc_pesquisador");
                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                    datanasc.setText(simpleDateFormat.format(data));
                    end.setText(sql.getString("tipo_endereco")+" "+sql.getString("logradouro")+" Num:"+sql.getString("numero")+" Bairro:"+sql.getString("bairro")+" Cidade:"+sql.getString("cidade")+" Estado:"+sql.getString("estado")+" Cep:"+sql.getString("cep")+" Complemento"+sql.getString("complemento"));
                }
                else if(questao==5){
                    
                    double salario=0.0;
                 
                    while (sql.next()) {                        
                        salario +=sql.getDouble(1);
                        
                    }
                    this.salario.setText(""+salario);
                    
                    
                }
                
                
                
            }sql.close();
        } catch (SQLException ex) {
            Logger.getLogger(Questao1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void fazbusca(){
    
    }
    private ResultSet busca1(){
        String sql="select * from projeto where data_fim is NULL "
                + "and departamento="+Integer.parseInt(codDep_tf.getText())+";";
        rs=pesqDAO.viewPesquisador(sql);
        return rs;
    }
    private ResultSet busca2(){
        String sql="SELECT COUNT(*) FROM projeto where departamento="+Integer.parseInt(codDep_tf.getText());
        rs=pesqDAO.viewPesquisador(sql);
        return rs;
        
    }
    private ResultSet busca3(){
        String sql="select P.nome_pesquisador, PHP.hora_semanal_pesquisador from pesquisador_has_projeto PHP, pesquisador P where PHP.pesquisador_id_pesquisador=id_pesquisador;";
        rs=pesqDAO.viewPesquisador(sql);
        return rs;
        
    }
    private ResultSet busca4(){
        String sql="select nome_pesquisador,data_nasc_pesquisador,tipo_endereco,EN.logradouro,EN.numero,complemento,EN.bairro,EN.estado,cep,EN.cidade "
                + "from pesquisador P, departamento D, endereco EN, pesquisador_has_endereco PHE "
                + "where P.id_pesquisador=PHE.id_pesquisador and PHE.numero=EN.numero and "
                + "PHE.bairro=EN.bairro and EN.logradouro=PHE.logradouro and PHE.cidade=EN.cidade and "
                + "D.gerente=P.id_pesquisador and D.cod_dep="+codDep_tf.getText();
        rs=pesqDAO.viewPesquisador(sql);
        return rs;
        
    }
    private ResultSet busca5(){
        String sql="select valor_vencimento from departamento D, pesquisador P, vencimento_pesquisador V where D.gerente=P.id_pesquisador and P.id_pesquisador=V.id_pesquisador and D.cod_dep="+codDep_tf.getText()+";";
        rs=pesqDAO.viewPesquisador(sql);
        return rs;
        
    }
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        painelPesq = new javax.swing.JScrollPane();
        jButton2 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        codDep_tf = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        resNumProj = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        painelPesq1 = new javax.swing.JScrollPane();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        nome_Ger = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        end = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        datanasc = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        salario = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jButton2.setText("Sair");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel3.setText("1.1- Quais projetos atualmente (que estão em andamento) ");

        codDep_tf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codDep_tfActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jairo/bancodedados/bd/imagem/1480986628_search.png"))); // NOI18N
        jButton4.setText("Buscar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setText("são controlados por aquele departamento");

        jLabel2.setText("1.2- Quantos projetos já passaram por aquele departamento ;");

        resNumProj.setEditable(false);

        jLabel4.setText("1.3- Quais são os funcionários (e suas  cargas horárias) ");

        jLabel5.setText("envolvidos em cada projeto por departamento;");

        jLabel6.setText("1.4- Qual o nome, o endereço, a data de nascimento ");

        jLabel7.setText("e o salário do gerente.");

        jLabel8.setText("Nome:");

        jLabel9.setText("Endereco:");

        jLabel10.setText("Data Nasc:");

        jLabel11.setText("Salario");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(painelPesq, javax.swing.GroupLayout.PREFERRED_SIZE, 469, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 12, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(painelPesq1, javax.swing.GroupLayout.PREFERRED_SIZE, 457, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(codDep_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton4)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(69, 69, 69)
                                .addComponent(jLabel1))
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(177, 177, 177)
                                .addComponent(resNumProj, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jLabel4))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(jLabel5))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(164, 164, 164)
                        .addComponent(jLabel7))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(jLabel6))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(4, 4, 4)
                                .addComponent(end))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nome_Ger, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10)
                                            .addComponent(jLabel11))
                                        .addGap(2, 2, 2)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(datanasc)
                                            .addComponent(salario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel3)
                .addGap(12, 12, 12)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codDep_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(painelPesq, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resNumProj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelPesq1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(12, 12, 12)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(nome_Ger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(end, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(datanasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(salario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        new LoginUser().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void codDep_tfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codDep_tfActionPerformed
        if (codDep_tf.getText().equals("")||codDep_tf.getText()==null) {
            JOptionPane.showMessageDialog(null, "Preencha o campo com um código \nde Departamento válido");
        }else{
            model.setRowCount(0);
            model1.setRowCount(0);
            preencheTable(busca1(),1);
            preencheTable(busca2(),2);
            preencheTable(busca3(),3);
            preencheTable(busca4(),4);
            preencheTable(busca5(),5);
        }

    }//GEN-LAST:event_codDep_tfActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        codDep_tfActionPerformed(evt);
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Questao1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Questao1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Questao1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Questao1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Questao1().setVisible(true);
            }
        });
    }/*
    private String getSQLFuncionario(){
        return "select nome_pesquisador, sexo_pesquisador, data_nasc_pesquisador, regime_de_trabalho_pesquisador, "
                + "email_pesquisador, area_atuacao_pesquisador,departamento, codigo_funcionario,nome_dep, login_usuario "
                + "from pesquisador P,departamento D "
                + "where codigo_funcionario ="+Integer.parseInt(codFuncio_tf.getText())+" and P.departamento=D.cod_dep;";
    }
    private String getSQLNome(){
        return "select nome_pesquisador, sexo_pesquisador, data_nasc_pesquisador, regime_de_trabalho_pesquisador, "
                + "email_pesquisador, area_atuacao_pesquisador,departamento, codigo_funcionario,nome_dep, login_usuario "
                + "from pesquisador P,departamento D "
                + "where nome_pesquisador like '%"+nomepesq.getText()+"%' and P.departamento=D.cod_dep;";
    }
    
    private String getSQLDep(){
        return "select nome_pesquisador, sexo_pesquisador, data_nasc_pesquisador, regime_de_trabalho_pesquisador, "
                + "email_pesquisador, area_atuacao_pesquisador,departamento, codigo_funcionario,nome_dep, login_usuario "
                + "from pesquisador P,departamento D "
                + "where departamento ="+Integer.parseInt(codDep_tf.getText())+" and P.departamento=D.cod_dep;";
    }*/
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField codDep_tf;
    private javax.swing.JTextField datanasc;
    private javax.swing.JTextField end;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField nome_Ger;
    private javax.swing.JScrollPane painelPesq;
    private javax.swing.JScrollPane painelPesq1;
    private javax.swing.JTextField resNumProj;
    private javax.swing.JTextField salario;
    // End of variables declaration//GEN-END:variables
}
