/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.GUI.secretario;

import jairo.bancodedados.bd.DAO.secretario.DependentesDAO;
import jairo.bancodedados.bd.DAO.secretario.EnderecoDAO;
import jairo.bancodedados.bd.beans.Secretario_has_endereco;
import jairo.bancodedados.bd.telas.menus.MenuAdmin;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class InsereEndereco extends javax.swing.JFrame {
    int flag=0;
    EnderecoDAO pceDAO=new EnderecoDAO();
    List<String> list=new ArrayList<>();
    /**
     * Creates new form PesquisadorCadastroEndereco
     */
    public InsereEndereco() {
        initComponents();
        setCidades();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(0);
        this.setSize(280, 380);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        codSec_tf = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbCidade = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        cbBairro = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        cbLogra = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        cbNum = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Cadastrar Endereço"));
        jPanel1.setLayout(new java.awt.GridLayout(0, 1));

        jLabel1.setText("Cod. Secretario:");
        jPanel1.add(jLabel1);

        codSec_tf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                codSec_tfKeyTyped(evt);
            }
        });
        jPanel1.add(codSec_tf);

        jLabel5.setText("Cidade:");
        jPanel1.add(jLabel5);

        cbCidade.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione" }));
        cbCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbCidadeActionPerformed(evt);
            }
        });
        jPanel1.add(cbCidade);

        jLabel3.setText("Bairro: ");
        jPanel1.add(jLabel3);

        cbBairro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione" }));
        cbBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbBairroActionPerformed(evt);
            }
        });
        jPanel1.add(cbBairro);

        jLabel4.setText("Logradouro:");
        jPanel1.add(jLabel4);

        cbLogra.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione" }));
        cbLogra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbLograActionPerformed(evt);
            }
        });
        jPanel1.add(cbLogra);

        jLabel2.setText("Número: ");
        jPanel1.add(jLabel2);

        cbNum.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione" }));
        cbNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNumActionPerformed(evt);
            }
        });
        jPanel1.add(cbNum);
        jPanel1.add(jLabel6);

        jButton1.setBackground(new java.awt.Color(13, 147, 32));
        jButton1.setText("Cadastrar Novo");
        jButton1.setOpaque(true);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);

        jButton2.setText("Finalizar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        flag=1;
        insereNoBanco();
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(flag==1){
            
            MenuAdmin adm=new MenuAdmin();
            adm.setVisible(true);
            String caminho="";
            //seleciona o sistema operacional

            String sisOper=System.getProperty("os.name");
            if (sisOper.equals("Linux")) {
                caminho="/tmp/cod.tmp";
            } else if(sisOper.equals("Windows")) {
                caminho="c://Windows/Temp/cod.tmp";
            } else{
                JOptionPane.showMessageDialog(null, "O programa não funciona nesse S.O");
            }
            //cria um arquivo temporario para armazenar o usuario e senha
            
            File file=new File(caminho);
            file.delete();
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(null, "Cadastre Pelo menos um Endereço");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void codSec_tfKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codSec_tfKeyTyped
        String caracteres="0987654321";
        if(!caracteres.contains(evt.getKeyChar()+"")){
            evt.consume();
        }
    }//GEN-LAST:event_codSec_tfKeyTyped

    private void cbCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbCidadeActionPerformed
        setBairrosPorCidade();
    }//GEN-LAST:event_cbCidadeActionPerformed

    private void cbBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbBairroActionPerformed
        setLogradouroPorBairro();
    }//GEN-LAST:event_cbBairroActionPerformed

    private void cbNumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNumActionPerformed
        
    }//GEN-LAST:event_cbNumActionPerformed

    private void cbLograActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbLograActionPerformed
        setNumeroPorLogradouro();
    }//GEN-LAST:event_cbLograActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InsereEndereco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InsereEndereco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InsereEndereco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InsereEndereco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InsereEndereco().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbBairro;
    private javax.swing.JComboBox<String> cbCidade;
    private javax.swing.JComboBox<String> cbLogra;
    private javax.swing.JComboBox<String> cbNum;
    private javax.swing.JTextField codSec_tf;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
    public void setCod(){
        getCod();
        codSec_tf.setEditable(false);
    }
    private void setCidades(){
        list=null;
        list=pceDAO.getCidade();
        for (int i = 0; i < list.size(); i++) {
            cbCidade.addItem(list.get(i)); 
        }
    }
    private void setBairrosPorCidade(){
        list=null;
        list=pceDAO.getBairroPorCidade(cbCidade.getSelectedItem().toString());
        for (int i = 0; i < list.size(); i++) {
            cbBairro.addItem(list.get(i)); 
        }
    }
    private void setLogradouroPorBairro(){
        list=null;
        list=pceDAO.getLogradouroPorBairro(cbBairro.getSelectedItem().toString());
        for (int i = 0; i < list.size(); i++) {
            cbLogra.addItem(list.get(i)); 
        }
    }
    private void setNumeroPorLogradouro(){
        list=null;
        list=pceDAO.getNumeroPorLogradouro(cbLogra.getSelectedItem().toString());
        System.out.println(cbLogra.getSelectedItem().toString()+"\n"+list.size()+"\n"+list.get(0));
        for (int i = 0; i < list.size(); i++) {
            cbNum.addItem(list.get(i)); 
        }
    }
    private void insereNoBanco(){
        DependentesDAO cdDAO=new DependentesDAO();
        Secretario_has_endereco endsec=new Secretario_has_endereco();
        pceDAO=new EnderecoDAO();
        
        endsec.setCidade(cbCidade.getSelectedItem().toString());
        endsec.setBairro(cbBairro.getSelectedItem().toString());
        endsec.setLogradouro(cbLogra.getSelectedItem().toString());
        endsec.setNum(cbNum.getSelectedItem().toString());
        endsec.setIdSecretario(cdDAO.getIdSecretario(Integer.parseInt(codSec_tf.getText()))); 
        pceDAO.insert(endsec);
        
    }
     private void getCod(){
        String caminho="";
        String sisOper=System.getProperty("os.name");
        if (sisOper.equals("Linux")) {
            caminho="/tmp/cod.tmp";
        } else if(sisOper.equals("Windows")) {
            caminho="c://Windows/Temp/cod.tmp";
        } else{
            JOptionPane.showMessageDialog(null, "O programa não funciona nesse S.O");
        }
        
        try(BufferedReader br=new BufferedReader(new FileReader(caminho))){
            codSec_tf.setText(br.readLine());
            
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
}
