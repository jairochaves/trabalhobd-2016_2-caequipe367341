/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO;

import jairo.bancodedados.bd.DAO.pesquisador.CadastroPesquisadorDAO;
import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.GUI.pesquisador.CadastroPesquisador;
import jairo.bancodedados.bd.beans.Departamento;
import jairo.bancodedados.bd.beans.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class DepartamentoDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement statement;
    public List<String> lDepCod=new ArrayList<>();
    public List<String> lDepNome=new ArrayList<>();
    
    public void listarCod(){
        inicializar();
        
        Departamento dep=new Departamento();
        String sql="SELECT cod_dep,nome_dep FROM departamento;";
        
        
        try {
            statement=connection.prepareStatement(sql);
            rs=statement.executeQuery();
            
        try {
            while(rs.next()){
                
                lDepCod.add(""+rs.getInt("cod_dep"));
                lDepNome.add(rs.getString("nome_dep"));
            }
            connection.close();
            rs.close();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisador.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Cadastre Primeiro um Endereco!!!");
        }
        
    }
    public void insere(Departamento dep){
        
        String sql0="INSERT INTO departamento ("
                + "cod_dep,"
                +"nome_dep,"
                + "gerente,"
                + "data_inicio_gestao,"
                + "data_fim_gestao"
                + ")VALUES(?,?,?,?,?);";
        inicializar();
        
        try {
            statement=connection.prepareStatement(sql0);
            
            
            statement.setInt(1, dep.getCodDep());
            statement.setString(2, dep.getNomeDep());
            if(dep.getGerenteDep()==0){
                statement.setNull(3, dep.getGerenteDep());
            }else{
                statement.setInt(3, dep.getGerenteDep());
            }
            statement.setDate(4, dep.getDataInicioGestao());
            statement.setDate(5, dep.getDataInicioGestao());
            
            statement.execute();
            
            connection.close();
            statement.close();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não Cadastrado, verifique os valores inseridos!!!");
            
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void inicializar(){
        connection=new Factory().getconection();
        lDepCod=new ArrayList<>();
        lDepNome=new ArrayList<>();
        
    }
}
