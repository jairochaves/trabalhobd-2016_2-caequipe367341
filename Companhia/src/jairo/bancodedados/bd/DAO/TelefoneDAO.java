/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO;

import jairo.bancodedados.bd.DAO.pesquisador.CadastroPesquisadorDAO;
import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Projeto;
import jairo.bancodedados.bd.beans.Telefone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class TelefoneDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement statement;
    //public List<Departamento> lDep=new ArrayList<>();
    
    public void insere(Telefone tel){
        
        String sql0="INSERT INTO telefone ("
                + "numero,"
                +"tipo_telefone)VALUES(?,?);";
        inicializar();
        
        try {
            statement=connection.prepareStatement(sql0);
            statement.setString(1, tel.getNumeroTelefone());
            statement.setString(2, tel.getTipoTelefone());
            
            
            statement.execute();
            
            connection.close();
            statement.close();
            
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não Cadastrado, verifique os valores inseridos!!!");
            
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void inicializar(){
        connection=new Factory().getconection();
//        lDep=new ArrayList<>();
    }
}
