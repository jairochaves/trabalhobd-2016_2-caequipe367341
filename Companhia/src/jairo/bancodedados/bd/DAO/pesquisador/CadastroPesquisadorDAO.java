package jairo.bancodedados.bd.DAO.pesquisador;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Pesquisador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class CadastroPesquisadorDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt,stmt2,stmt0;
    
    //verifica se o usuário e senha estão corretos
    public boolean loginCorreto(String user,String password){
        //String url="select * from login where usuario='"+user+"' and senha='"+password+"';";
        String url="select * from login where usuario='"+user+"' and senha=password('"+password+"');";//inserir a função password() para a senha
        int cont=0;
        boolean logou=false;
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                cont++;
            }
            
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(cont==1){
            logou=true;
        }
        return logou;
    }
    public String getCodUserComum(String user){
        String url="SELECT C.codigo_funcionario FROM pesquisador P, "
                + "codigo_funcionario C,login L WHERE L.usuario='"+user+"' "
                + "and P.codigo_funcionario=C.codigo_funcionario ;";
        String codGerente="";
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                codGerente=rs.getString("codigo_funcionario");
            }
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return codGerente;
    }
    public boolean inserePesquisador(Pesquisador pesq){
        boolean cadastrou=false;
        String sql0="INSERT INTO codigo_funcionario VALUES(?);";
        String sql="INSERT INTO pesquisador "+
                "(nome_pesquisador,"
                + "sexo_pesquisador,"
                + "data_nasc_pesquisador,"
                + "codigo_funcionario,"
                + "regime_de_trabalho_pesquisador,"
                + "area_atuacao_pesquisador,"
                + "email_pesquisador,"
                + "departamento,"
                + "login_usuario) VALUES(?,?,?,?,?,?,?,?,?);";
        String sql2="INSERT INTO login "
                + "(usuario,senha) VALUES(?,password(?));";
        
        openConection();
        
        try {
            stmt0=connection.prepareStatement(sql0);
            stmt=connection.prepareStatement(sql);
            stmt2=connection.prepareStatement(sql2);
            
            stmt0.setInt(1, pesq.getIdPesquisador());
            
            stmt.setString(1, pesq.getNomePesquisador());
            stmt.setString(2, pesq.getSexo());
            stmt.setDate  (3, pesq.getDataNascPesquisador());
            stmt.setInt   (4, pesq.getIdPesquisador());
            stmt.setString(5, pesq.getRegimeDeTrabalhoPesquisador());
            stmt.setString(6, pesq.getAreaDeAtuacao());
            stmt.setString(7, pesq.getEmail());
            stmt.setInt   (8, pesq.getDeparamento());
            stmt.setString(9, pesq.getLogin_usuario());
            
            stmt2.setString(1, pesq.getLogin_usuario());
            stmt2.setString(2, pesq.getSenha());
            
            stmt0.execute();
            stmt2.execute();
            stmt.execute();
            
            
            connection.close();
            
            stmt.close();
            stmt0.close();
            stmt2.close();
            
            cadastrou=true;
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            System.err.println("Erro ao cadastrar Pesquisador");
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cadastrou;
    }
//abre uma conexão com o banco
    private void openConection(){
        connection=new Factory().getconection();
    }
    public ResultSet viewPesquisador(String comando){
        openConection();
        //String sql="select * from pesquisador";
        try {
            stmt=connection.prepareStatement(comando);
            rs=stmt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
}
