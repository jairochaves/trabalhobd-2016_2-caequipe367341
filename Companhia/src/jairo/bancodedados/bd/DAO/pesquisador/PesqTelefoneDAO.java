/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.pesquisador;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.GUI.pesquisador.CadastroPesquisador;
import jairo.bancodedados.bd.beans.Pesquisador_has_telefone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class PesqTelefoneDAO {
    public List<String> lcpesqcel=new ArrayList<String>();
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public List<String> getNumCel(){
        lcpesqcel=new ArrayList<String>();
        
        String sql="select numero from telefone;";
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        try {
            while(rs.next()){
                lcpesqcel.add(""+rs.getString("numero"));
                
            }
            connection.close();
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisador.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Cadastre Primeiro um Endereco!!!");
        }
        
        return lcpesqcel;
    }
    public int getCodTel(String num){
        String sql="SELECT id_telefone FROM telefone where numero='"+num+"';";
        int id=0;
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
            while (rs.next()){
                id=rs.getInt("id_telefone");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PesqTelefoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
    }
    public void insere(Pesquisador_has_telefone tel){
        
        String sql0="INSERT INTO pesquisador_has_telefone ("
                + "pesquisador,"
                +"telefone_pesquisador)VALUES(?,?);";
        
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql0);
            
            stmt.setInt(1, tel.getCodPesquisador());
            stmt.setInt(2, tel.getCodTelefone());
            
            stmt.execute();
            
            connection.close();
            stmt.close();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao cadastrar Telefone");
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    private void openConection(){
        connection=new Factory().getconection();
    }
}
