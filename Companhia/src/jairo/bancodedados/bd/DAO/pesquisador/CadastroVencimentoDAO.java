/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.pesquisador;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Pesquisador_has_telefone;
import jairo.bancodedados.bd.beans.Vencimento_Pesquisador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class CadastroVencimentoDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public void insert(Vencimento_Pesquisador vp){
        String sql="INSERT INTO vencimento_pesquisador ("
                + "cod_vencimento,"
                + "descricao_vencimento,"
                + "valor_vencimento,"
                + "id_pesquisador"
                + ") VALUES (?,?,?,?);";
        openConection();
        try {
            stmt=connection.prepareStatement(sql);
            
            stmt.setInt(1, vp.getCodVencimento());
            stmt.setString(2, vp.getDescricaoVencimento());
            stmt.setDouble(3, vp.getValorVencimento());
            stmt.setInt(4, vp.getIdPesquisador());
            
            stmt.execute();
            
            stmt.close();
            connection.close();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Algum erro ocorreu durante o cadastramento!");
            Logger.getLogger(PesquisadorCadastroEnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
        
    
    private void openConection(){
        connection=new Factory().getconection();
    }
}
