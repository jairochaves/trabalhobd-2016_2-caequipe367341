/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.pesquisador;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.GUI.pesquisador.CadastroPesquisador;
import jairo.bancodedados.bd.beans.Departamento;
import jairo.bancodedados.bd.beans.Dependente_pesquisador;
import jairo.bancodedados.bd.beans.Pesquisador_has_Projeto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class CadastroPesqEmProjetoDAO {
    public List<String> lphp=new ArrayList<String>();
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
        
    
    public void insere(Pesquisador_has_Projeto php){
        
        String sql0="INSERT INTO pesquisador_has_projeto ("
                + "pesquisador_id_pesquisador,"
                +"projeto_cod_projeto,"
                + "hora_semanal_pesquisador)VALUES(?,?,?);";
        
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql0);
            
            
            stmt.setInt   (1, php.getCodPesquisador());
            stmt.setInt   (2, php.getCodProjeto());
            stmt.setString(3, php.getHorasSemanaisPesquisador());
            
            
            
            
            stmt.execute();
            
            
            connection.close();
            
            stmt.close();
            
            
            
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            System.err.println("Erro ao cadastrar Pesquisador");
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public List<String> getCodProjetos(){
        lphp=new ArrayList<String>();
        
        String sql="select cod_projeto from projeto;";
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        try {
            while(rs.next()){
                lphp.add(""+rs.getInt("cod_projeto"));
                
            }
            connection.close();
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisador.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Cadastre Primeiro um Endereco!!!");
        }
        
        return lphp;
    }
    public String mostraNome(int cod){
        String sql="select nome_projeto from projeto where cod_projeto="+cod+";";
        String nome="";
        openConection();
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            while(rs.next()){
                nome=rs.getString("nome_projeto");
            }
            connection.close();
            stmt.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(CadastroPesqEmProjetoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return nome;
    }
    private void openConection(){
        connection=new Factory().getconection();
    }
}
