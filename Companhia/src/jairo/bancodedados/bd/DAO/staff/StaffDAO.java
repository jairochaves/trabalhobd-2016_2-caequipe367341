package jairo.bancodedados.bd.DAO.staff;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Staff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class StaffDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt,stmt2,stmt0;
    
    //verifica se o usuário e senha estão corretos
    public boolean loginCorreto(String user,String password){
        //String url="select * from login where usuario='"+user+"' and senha='"+password+"';";
        String url="select * from login where usuario='"+user+"' and senha=password('"+password+"');";//inserir a função password() para a senha
        int cont=0;
        boolean logou=false;
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                cont++;
            }
            
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(cont==1){
            logou=true;
        }
        return logou;
    }
    public String getCodUserComum(String user){
        String url="SELECT C.codigo_funcionario FROM staff P, "
                + "codigo_funcionario C,login L WHERE L.usuario='"+user+"' "
                + "and P.codigo_funcionario=C.codigo_funcionario ;";
        String codGerente="";
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                codGerente=rs.getString("codigo_funcionario");
            }
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return codGerente;
    }
    public boolean insere(Staff staff){
        boolean cadastrou=false;
        String sql0="INSERT INTO codigo_funcionario VALUES(?);";
        String sql="INSERT INTO staff "+
                "(nome_staff,"
                + "sexo_staff,"
                + "data_nasc_staff,"
                + "funcao_staff,"
                + "regime_trabalho_staff,"
                + "jornada_trabalho_staff,"
                + "supervisor,"
                + "departamento,"
                + "codigo_funcionario,"
                + "login_usuario) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?);";
        String sql2="INSERT INTO login "
                + "(usuario,senha) VALUES(?,password(?));";
        
        openConection();
        
        try {
            stmt0=connection.prepareStatement(sql0);
            stmt=connection.prepareStatement(sql);
            stmt2=connection.prepareStatement(sql2);
            
            stmt0.setInt(1, staff.getIdStaff());
            
            stmt.setString(1, staff.getNomeStaff());
            stmt.setString(2, staff.getSexo());
            stmt.setDate  (3, staff.getDataNascStaff());
            stmt.setString(4, staff.getFuncao());
            stmt.setString(5, staff.getRegimeDeTrabalho());
            stmt.setString(6, staff.getJornadaDeTrabalho());
            stmt.setInt   (7, staff.getIdSupervisor());
            stmt.setInt   (8, staff.getDepartamento());
            stmt.setInt   (9, staff.getIdStaff());
            stmt.setString(10,staff.getLogin());
            
            stmt2.setString(1, staff.getLogin());
            stmt2.setString(2, staff.getSenha());
            
            stmt0.execute();
            stmt2.execute();
            stmt.execute();
            
            
            connection.close();
            
            stmt.close();
            stmt0.close();
            stmt2.close();
            
            cadastrou=true;
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar!!!\n\n"+ex.getMessage());
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cadastrou;
    }
//abre uma conexão com o banco
    private void openConection(){
        connection=new Factory().getconection();
    }
    
}
