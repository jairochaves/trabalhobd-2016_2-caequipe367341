/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.staff;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.GUI.staff.InsereStaff;
import jairo.bancodedados.bd.beans.Staff_has_endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class EnderecoDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public List getCidade(){
        opemConection();
        
        String sql="SELECT cidade FROM endereco";
        List<String> list=new ArrayList<>();
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        
            while(rs.next()){
                list.add(rs.getString("cidade"));
            }
            connection.close();
            rs.close();
            stmt.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List getBairroPorCidade(String cidade){
        opemConection();
        
        String sql="SELECT bairro FROM endereco WHERE cidade='"+cidade+"';";
        
        List<String> list=new ArrayList<>();
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        
            while(rs.next()){
                list.add(rs.getString("bairro"));
            }
            connection.close();
            rs.close();
            stmt.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Cadastre Primeiro um Endereco!!!");
        }
        
        
        return list;
    }
    public List getLogradouroPorBairro(String bairro){
        opemConection();
        
        String sql="SELECT logradouro FROM endereco where bairro='"+bairro+"';";
        List<String> list=new ArrayList<>();
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        
            while(rs.next()){
                list.add(rs.getString("logradouro"));
            }
            connection.close();
            rs.close();
            stmt.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List getNumeroPorLogradouro(String logradouro){
        opemConection();
        
        String sql="SELECT numero FROM endereco where logradouro='"+logradouro+"';";
        List<String> list=new ArrayList<>();
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        try {
            while(rs.next()){
                list.add(rs.getString("numero"));
            }
            connection.close();
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(InsereStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return list;
    }
    public void insert(Staff_has_endereco pe){
        String sql="INSERT INTO staff_has_endereco ("
                + "id_staff,"
                + "cidade,"
                + "bairro,"
                + "logradouro,"
                + "numero"
                + ") VALUES (?,?,?,?,?);";
        opemConection();
        try {
            stmt=connection.prepareStatement(sql);
            
            stmt.setInt(1, pe.getIdStaff());
            stmt.setString(2, pe.getCidade());
            stmt.setString(3, pe.getBairro());
            stmt.setString(4, pe.getLogradouro());
            stmt.setString(5, pe.getNumero());
            stmt.execute();
            
            stmt.close();
            connection.close();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Algum erro ocorreu durante o cadastramento!");
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    private void opemConection(){
        connection=new Factory().getconection();
    }
    
}
