/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.staff;

import jairo.bancodedados.bd.DAO.staff.*;
import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Staff_has_telefone;
import jairo.bancodedados.bd.beans.Vencimento_staff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class VencimentoDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public void insert(Vencimento_staff vp){
        String sql="INSERT INTO vencimento_staff ("
                + "cod_vencimento,"
                + "descricao_vencimento,"
                + "valor_vencimento,"
                + "id_staff"
                + ") VALUES (?,?,?,?);";
        openConection();
        try {
            stmt=connection.prepareStatement(sql);
            
            stmt.setInt(1, vp.getCodVencimento());
            stmt.setString(2, vp.getDescricaoVencimento());
            stmt.setDouble(3, vp.getValorVencimento());
            stmt.setInt(4, vp.getIdStaff());
            
            stmt.execute();
            
            stmt.close();
            connection.close();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Algum erro ocorreu durante o cadastramento!");
            Logger.getLogger(EnderecoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
        
    
    private void openConection(){
        connection=new Factory().getconection();
    }
}
