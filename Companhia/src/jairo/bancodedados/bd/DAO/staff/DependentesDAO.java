/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.staff;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Dependente_staff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class DependentesDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public void insere(Dependente_staff dp){
        
        String sql0="INSERT INTO dependente_staff ("
                + "nome_dependente,"
                +"sexo_dependente,"
                + "data_nasc_dependente,"
                + "grau_parentesco_dependente,"
                + "id_staff"
                + ")VALUES(?,?,?,?,?);";
        
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql0);
            
            stmt.setString(1, dp.getNomeDependente());
            stmt.setString(2, dp.getSexoDependente());
            stmt.setDate  (3, dp.getDataNascDependente());
            stmt.setString(4, dp.getGrauParentesco());
            stmt.setInt   (5, dp.getIdStaff());
            
            
            
            
            stmt.execute();
            
            
            connection.close();
            
            stmt.close();
            
            
            
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar!\n\n"+ex.getMessage());
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public int getIdStaff(int Cod){
        String url="select id_staff from staff where codigo_funcionario='"+Cod+"';";//inserir a função password() para a senha
        int id=0;
        
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                id=rs.getInt("id_staff");
            }
            
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(StaffDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        return id;
    }
    //abre uma conexão com o banco
    private void openConection(){
        connection=new Factory().getconection();
    }
}
