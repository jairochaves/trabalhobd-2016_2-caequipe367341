/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO;

import jairo.bancodedados.bd.DAO.pesquisador.CadastroPesquisadorDAO;
import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Dependente_pesquisador;
import jairo.bancodedados.bd.beans.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class EnderecoDAO {
    Connection connection=new Factory().getconection();
    PreparedStatement statement;
    public void insere(Endereco end){
        
        String sql0="INSERT INTO endereco ("
                + "tipo_endereco,"
                +"logradouro,"
                + "numero,"
                + "complemento,"
                + "bairro,"
                + "cidade,"
                + "estado,"
                + "cep"
                + ")VALUES(?,?,?,?,?,?,?,?);";
        openConection();
        
        try {
            statement=connection.prepareStatement(sql0);
            statement.setString(1, end.getTipoEndereco());
            statement.setString(2, end.getLogradouro());
            statement.setString(3, end.getNumero());
            statement.setString(4, end.getComplemento());
            statement.setString(5, end.getBairro());
            statement.setString(6, end.getCidade());
            statement.setString(7, end.getEstado());
            statement.setString(8, end.getCep());
            
            statement.execute();
            
            connection.close();
            statement.close();
            
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não Cadastrado, verifique os valores inseridos!!!");
            
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void openConection(){
        connection=new Factory().getconection();
    }
}
