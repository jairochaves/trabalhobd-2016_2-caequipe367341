/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO;

import jairo.bancodedados.bd.DAO.pesquisador.CadastroPesquisadorDAO;
import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Projeto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class ProjetoDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement statement;
    //public List<Departamento> lDep=new ArrayList<>();
    
    public void insere(Projeto proj){
        
        String sql0="INSERT INTO projeto ("
                + "cod_projeto,"
                +"nome_projeto,"
                + "data_inicio,"
                + "data_fim,"
                + "departamento"
                + ")VALUES(?,?,?,?,?);";
        inicializar();
        
        try {
            statement=connection.prepareStatement(sql0);
            statement.setInt(1, proj.getCodProjeto());
            statement.setString(2, proj.getNomeProjeto());
            statement.setDate(3, proj.getDataInicio());
            statement.setDate(4, proj.getDataFim());
            statement.setInt(5, proj.getDepartamento());
            
            
            statement.execute();
            
            connection.close();
            statement.close();
            
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Não Cadastrado, verifique os valores inseridos!!!");
            
            Logger.getLogger(CadastroPesquisadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void inicializar(){
        connection=new Factory().getconection();
//        lDep=new ArrayList<>();
    }
}
