package jairo.bancodedados.bd.DAO.secretario;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.GUI.secretario.InsereSecretario;
import jairo.bancodedados.bd.beans.Secretario_has_telefone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class TelefoneDAO {
    public List<String> lcseccel=new ArrayList<>();
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public List<String> getNumCel(){
        lcseccel=new ArrayList<>();
        
        String sql="select numero from telefone;";
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
        try {
            while(rs.next()){
                lcseccel.add(""+rs.getString("numero"));
                
            }
            connection.close();
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(InsereSecretario.class.getName()).log(Level.SEVERE, null, ex);
        }
        } catch (SQLException ex) {
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Cadastre Primeiro um Endereco!!!");
        }
        
        return lcseccel;
    }
    public int getCodTel(String num){
        String sql="SELECT id_telefone FROM telefone where numero='"+num+"';";
        int id=0;
        try {
            stmt=connection.prepareStatement(sql);
            rs=stmt.executeQuery();
            
            while (rs.next()){
                id=rs.getInt("id_telefone");
            }
        } catch (SQLException ex) {
            Logger.getLogger(TelefoneDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
    }
    public void insere(Secretario_has_telefone tel){
        
        String sql0="INSERT INTO secretario_has_telefone ("
                + "secretario,"
                +"telefone_secretario)VALUES(?,?);";
        
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql0);
            
            stmt.setInt(1, tel.getSecretario());
            stmt.setInt(2, tel.getCodTelefone());
            
            stmt.execute();
            
            connection.close();
            stmt.close();
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao cadastrar Telefone");
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    private void openConection(){
        connection=new Factory().getconection();
    }
}
