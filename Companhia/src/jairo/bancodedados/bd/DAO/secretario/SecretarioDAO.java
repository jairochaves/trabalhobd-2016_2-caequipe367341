package jairo.bancodedados.bd.DAO.secretario;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Secretario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class SecretarioDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt,stmt2,stmt0;
    
    //verifica se o usuário e senha estão corretos
    public boolean loginCorreto(String user,String password){
        //String url="select * from login where usuario='"+user+"' and senha='"+password+"';";
        String url="select * from login where usuario='"+user+"' and senha=password('"+password+"');";//inserir a função password() para a senha
        int cont=0;
        boolean logou=false;
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                cont++;
            }
            
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(cont==1){
            logou=true;
        }
        return logou;
    }
    public String getCodUserComum(String user){
        String url="SELECT C.codigo_funcionario FROM secretario P, "
                + "codigo_funcionario C,login L WHERE L.usuario='"+user+"' "
                + "and P.codigo_funcionario=C.codigo_funcionario ;";
        String codGerente="";
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                codGerente=rs.getString("codigo_funcionario");
            }
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return codGerente;
    }
    public boolean insere(Secretario sec){
        boolean cadastrou=false;
        String sql0="INSERT INTO codigo_funcionario VALUES(?);";
        String sql="INSERT INTO secretario "+
                "(nome_secretario,"
                + "sexo_secretario,"
                + "data_nasc_secretario,"
                + "codigo_funcionario,"
                + "regime_trabalho_secretario,"
                + "grau_escol_secretario,"
                + "departamento,"
                + "login_usuario) VALUES(?,?,?,?,?,?,?,?);";
        String sql2="INSERT INTO login "
                + "(usuario,senha) VALUES(?,password(?));";
        
        openConection();
        
        try {
            stmt0=connection.prepareStatement(sql0);
            stmt=connection.prepareStatement(sql);
            stmt2=connection.prepareStatement(sql2);
            
            stmt0.setInt(1, sec.getIdSecretario());
            
            stmt.setString(1, sec.getNomeSecretario());
            stmt.setString(2, sec.getSexo());
            stmt.setDate  (3, sec.getDataNascSecretario());
            stmt.setInt   (4, sec.getIdSecretario());
            stmt.setString(5, sec.getRegimeDeTrabalho());
            stmt.setString(6, sec.getGrauEscolarSecretario());
            stmt.setInt   (7, sec.getDepartamento());
            stmt.setString(8, sec.getLogin());
            
            stmt2.setString(1, sec.getLogin());
            stmt2.setString(2, sec.getSenha());
            
            stmt0.execute();
            stmt2.execute();
            stmt.execute();
            
            
            connection.close();
            
            stmt.close();
            stmt0.close();
            stmt2.close();
            
            cadastrou=true;
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir, verifique os valores fornecidos!");
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cadastrou;
    }
//abre uma conexão com o banco
    private void openConection(){
        connection=new Factory().getconection();
    }
    
}
