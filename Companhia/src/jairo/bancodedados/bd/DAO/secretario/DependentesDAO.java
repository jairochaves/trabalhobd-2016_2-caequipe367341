/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.DAO.secretario;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.beans.Dependente_secretario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class DependentesDAO {
    Connection connection=new Factory().getconection();
    ResultSet rs;
    PreparedStatement stmt;
    
    public void insere(Dependente_secretario ds){
        
        String sql0="INSERT INTO dependente_secretario ("
                + "nome_dependente,"
                +"sexo_dependente,"
                + "data_nasc_dependente,"
                + "grau_parentesco_dependente,"
                + "id_secretario"
                + ")VALUES(?,?,?,?,?);";
        
        
        openConection();
        
        try {
            stmt=connection.prepareStatement(sql0);
            
            stmt.setString(1, ds.getNomeDependente());
            stmt.setString(2, ds.getSexoDependente());
            stmt.setDate  (3, ds.getDataNascDependente());
            stmt.setString(4, ds.getGrauParentesco());
            stmt.setInt   (5, ds.getIdSecretario());
            
            
            
            
            stmt.execute();
            
            
            connection.close();
            
            stmt.close();
            
            
            
            JOptionPane.showMessageDialog(null, "Cadastrado com sucesso");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "erro ao cadastrar!");
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public int getIdSecretario(int CodSec){
        String url="select id_secretario from secretario where codigo_funcionario="+CodSec+";";//inserir a função password() para a senha
        int id=0;
        
        openConection();
        try {
            stmt=connection.prepareStatement(url);
            rs=stmt.executeQuery();
            while(rs.next()){
                id=rs.getInt("id_secretario");
            }
            
            rs.close();
            stmt.close();
            connection.close();
        
        } catch (SQLException ex) {
            Logger.getLogger(SecretarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        return id;
    }
    //abre uma conexão com o banco
    private void openConection(){
        connection=new Factory().getconection();
    }
}
