
package jairo.bancodedados.bd.beans;

import java.sql.Date;

/**
 *
 * @author jairochaves
 */
public class Departamento {
    private int codDep;
    private String nomeDep;
    private int gerenteDep;
    private Date dataInicioGestao;
    private Date dataFimGestao;

    public int getCodDep() {
        return codDep;
    }

    public void setCodDep(int codDep) {
        this.codDep = codDep;
    }

    public String getNomeDep() {
        return nomeDep;
    }

    public void setNomeDep(String nomeDep) {
        this.nomeDep = nomeDep;
    }

    public int getGerenteDep() {
        return gerenteDep;
    }

    public void setGerenteDep(int gerenteDep) {
        this.gerenteDep = gerenteDep;
    }

    public Date getDataInicioGestao() {
        return dataInicioGestao;
    }

    public void setDataInicioGestao(Date dataInicioGestao) {
        this.dataInicioGestao = dataInicioGestao;
    }

    public Date getDataFimGestao() {
        return dataFimGestao;
    }

    public void setDataFimGestao(Date dataFimGestao) {
        this.dataFimGestao = dataFimGestao;
    }
}
