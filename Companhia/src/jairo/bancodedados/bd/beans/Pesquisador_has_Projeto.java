/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Pesquisador_has_Projeto {
    private int codProjeto;
    private int codPesquisador;
    private String horasSemanaisPesquisador;
    
    public int getCodProjeto() {
        return codProjeto;
 
    }

    public void setCodProjeto(int codProjeto) {
        this.codProjeto = codProjeto;
    }

    public int getCodPesquisador() {
        return codPesquisador;
    }

    public void setCodPesquisador(int codPesquisador) {
        this.codPesquisador = codPesquisador;
    }

    public String getHorasSemanaisPesquisador() {
        return horasSemanaisPesquisador;
    }

    public void setHorasSemanaisPesquisador(String horasSemanaisPesquisador) {
        this.horasSemanaisPesquisador = horasSemanaisPesquisador;
    }
    
    
}
