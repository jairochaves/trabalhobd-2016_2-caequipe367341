package jairo.bancodedados.bd.beans;

import java.sql.Date;

/**
 *
 * @author jairochaves
 */
public class Pesquisador {
    private int idPesquisador;
    private String nomePesquisador;
    private String sexo;              //M ou F
    private Date dataNascPesquisador;
    private String regimeDeTrabalhoPesquisador; //'estatutário','celetista'
    private String email;
    private String areaDeAtuacao;
    private int deparamento;
    private String login_usuario;
    private String senha;
    public int getIdPesquisador() {
        return idPesquisador;
    }

    public void setIdPesquisador(int idPesquisador) {
        this.idPesquisador = idPesquisador;
    }

    public String getNomePesquisador() {
        return nomePesquisador;
    }

    public void setNomePesquisador(String nomePesquisador) {
        this.nomePesquisador = nomePesquisador;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascPesquisador() {
        return dataNascPesquisador;
    }

    public void setDataNascPesquisador(Date dataNascPesquisador) {
        this.dataNascPesquisador = dataNascPesquisador;
    }

    public String getRegimeDeTrabalhoPesquisador() {
        return regimeDeTrabalhoPesquisador;
    }

    public void setRegimeDeTrabalhoPesquisador(String regimeDeTrabalhoPesquisador) {
        this.regimeDeTrabalhoPesquisador = regimeDeTrabalhoPesquisador;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAreaDeAtuacao() {
        return areaDeAtuacao;
    }

    public void setAreaDeAtuacao(String areaDeAtuacao) {
        this.areaDeAtuacao = areaDeAtuacao;
    }

    public int getDeparamento() {
        return deparamento;
    }

    public void setDeparamento(int deparamento) {
        this.deparamento = deparamento;
    }

    public String getLogin_usuario() {
        return login_usuario;
    }

    public void setLogin_usuario(String login_usuario) {
        this.login_usuario = login_usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
}
