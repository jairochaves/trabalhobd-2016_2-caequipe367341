package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Vencimento_staff {
    private int codVencimento;
    private String descricaoVencimento;
    private Double valorVencimento;
    private Double salarioLiquido;
    private int idStaff;

    public int getCodVencimento() {
        return codVencimento;
    }

    public void setCodVencimento(int codVencimento) {
        this.codVencimento = codVencimento;
    }

    public String getDescricaoVencimento() {
        return descricaoVencimento;
    }

    public void setDescricaoVencimento(String descricaoVencimento) {
        this.descricaoVencimento = descricaoVencimento;
    }

    public Double getValorVencimento() {
        return valorVencimento;
    }

    public void setValorVencimento(Double valorVencimento) {
        this.valorVencimento = valorVencimento;
    }

    public Double getSalarioLiquido() {
        return salarioLiquido;
    }

    public void setSalarioLiquido(Double salarioLiquido) {
        this.salarioLiquido = salarioLiquido;
    }

    public int getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(int idStaff) {
        this.idStaff = idStaff;
    }
            
  
}
