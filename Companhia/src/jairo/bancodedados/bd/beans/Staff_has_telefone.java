package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Staff_has_telefone {
    private int idStaff;
    private int codTelefone;

    public int getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(int idStaff) {
        this.idStaff = idStaff;
    }

    public int getCodTelefone() {
        return codTelefone;
    }

    public void setCodTelefone(int codTelefone) {
        this.codTelefone = codTelefone;
    }
}
