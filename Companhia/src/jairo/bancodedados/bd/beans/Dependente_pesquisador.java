/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.beans;

import java.sql.Date;

/**
 *
 * @author jairochaves
 */
public class Dependente_pesquisador {
    private int codDependente;
    private String nomeDependente;
    private String sexoDependente;
    private Date dataNascDependente;
    private String grauParentesco;
    private Double salarioDependente;
    private int idPesquisador;

    public int getCodDependente() {
        return codDependente;
    }

    public void setCodDependente(int codDependente) {
        this.codDependente = codDependente;
    }

    public String getNomeDependente() {
        return nomeDependente;
    }

    public void setNomeDependente(String nomeDependente) {
        this.nomeDependente = nomeDependente;
    }

    public String getSexoDependente() {
        return sexoDependente;
    }

    public void setSexoDependente(String sexoDependente) {
        this.sexoDependente = sexoDependente;
    }

    public Date getDataNascDependente() {
        return dataNascDependente;
    }

    public void setDataNascDependente(Date dataNascDependente) {
        this.dataNascDependente = dataNascDependente;
    }

    public String getGrauParentesco() {
        return grauParentesco;
    }

    public void setGrauParentesco(String grauParentesco) {
        this.grauParentesco = grauParentesco;
    }

    public Double getSalarioDependente() {
        return salarioDependente;
    }

    public void setSalarioDependente(Double salarioDependente) {
        this.salarioDependente = salarioDependente;
    }

    public int getIdPesquisador() {
        return idPesquisador;
    }

    public void setIdPesquisador(int idPesquisador) {
        this.idPesquisador = idPesquisador;
    }
}
