package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Vencimento_Pesquisador {
    private int codVencimento;
    private String descricaoVencimento;
    private Double valorVencimento;
    private Double SalarioLiquido;
    private int idPesquisador;

    public int getCodVencimento() {
        return codVencimento;
    }

    public void setCodVencimento(int codVencimento) {
        this.codVencimento = codVencimento;
    }

    public String getDescricaoVencimento() {
        return descricaoVencimento;
    }

    public void setDescricaoVencimento(String descricaoVencimento) {
        this.descricaoVencimento = descricaoVencimento;
    }

    public Double getValorVencimento() {
        return valorVencimento;
    }

    public void setValorVencimento(Double valorVencimento) {
        this.valorVencimento = valorVencimento;
    }

    public Double getSalarioLiquido() {
        return SalarioLiquido;
    }

    public void setSalarioLiquido(Double SalarioLiquido) {
        this.SalarioLiquido = SalarioLiquido;
    }

    public int getIdPesquisador() {
        return idPesquisador;
    }

    public void setIdPesquisador(int idPesquisador) {
        this.idPesquisador = idPesquisador;
    }
    
         
}
