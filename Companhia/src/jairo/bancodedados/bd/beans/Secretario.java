/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.beans;

import java.sql.Date;

/**
 *
 * @author jairochaves
 */
public class Secretario {
    private int idSecretario;
    private String nomeSecretario;
    private String sexo;                  //M ou F
    private Date dataNascSecretario;
    private String regimeDeTrabalho;    //'estatutário','celetista','horista'
    private String login;
    private String senha;
    private String grauEscolarSecretario;
    private int departamento;

    public int getIdSecretario() {
        return idSecretario;
    }

    public void setIdSecretario(int idSecretario) {
        this.idSecretario = idSecretario;
    }

    public String getNomeSecretario() {
        return nomeSecretario;
    }

    public void setNomeSecretario(String nomeSecretario) {
        this.nomeSecretario = nomeSecretario;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascSecretario() {
        return dataNascSecretario;
    }

    public void setDataNascSecretario(Date dataNascSecretario) {
        this.dataNascSecretario = dataNascSecretario;
    }

    public String getRegimeDeTrabalho() {
        return regimeDeTrabalho;
    }

    public void setRegimeDeTrabalho(String regimeDeTrabalho) {
        this.regimeDeTrabalho = regimeDeTrabalho;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getGrauEscolarSecretario() {
        return grauEscolarSecretario;
    }

    public void setGrauEscolarSecretario(String grauEscolarSecretario) {
        this.grauEscolarSecretario = grauEscolarSecretario;
    }

    public int getDepartamento() {
        return departamento;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }
    
}
