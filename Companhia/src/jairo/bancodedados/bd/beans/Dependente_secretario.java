package jairo.bancodedados.bd.beans;

import java.sql.Date;

/**
 *
 * @author jairochaves
 */
public class Dependente_secretario {
    private int cod_dependente;
    private String nomeDependente;
    private String sexoDependente;
    private Date dataNascDependente;
    private String grauParentesco;
    private int idSecretario;
    private Double salarioDependente;

    public int getCod_dependente() {
        return cod_dependente;
    }

    public void setCod_dependente(int cod_dependente) {
        this.cod_dependente = cod_dependente;
    }

    public String getNomeDependente() {
        return nomeDependente;
    }

    public void setNomeDependente(String nomeDependente) {
        this.nomeDependente = nomeDependente;
    }

    public String getSexoDependente() {
        return sexoDependente;
    }

    public void setSexoDependente(String sexoDependente) {
        this.sexoDependente = sexoDependente;
    }

    public Date getDataNascDependente() {
        return dataNascDependente;
    }

    public void setDataNascDependente(Date dataNascDependente) {
        this.dataNascDependente = dataNascDependente;
    }

    public String getGrauParentesco() {
        return grauParentesco;
    }

    public void setGrauParentesco(String grauParentesco) {
        this.grauParentesco = grauParentesco;
    }

    public int getIdSecretario() {
        return idSecretario;
    }

    public void setIdSecretario(int idSecretario) {
        this.idSecretario = idSecretario;
    }

    public Double getSalarioDependente() {
        return salarioDependente;
    }

    public void setSalarioDependente(Double salarioDependente) {
        this.salarioDependente = salarioDependente;
    }
    
    
}
