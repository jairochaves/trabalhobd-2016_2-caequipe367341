/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Secretario_has_telefone {
    private int secretario;
    private int codTelefone;

    public int getSecretario() {
        return secretario;
    }

    public void setSecretario(int secretario) {
        this.secretario = secretario;
    }

    public int getCodTelefone() {
        return codTelefone;
    }

    public void setCodTelefone(int codTelefone) {
        this.codTelefone = codTelefone;
    }
}
