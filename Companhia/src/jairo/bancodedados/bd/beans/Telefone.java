package jairo.bancodedados.bd.beans;


/**
 *
 * @author jairochaves
 */
public class Telefone {
    private String numeroTelefone;
    private String tipoTelefone;
    
    public Telefone(){
        numeroTelefone="";
        tipoTelefone="";
    }

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }

    public String getTipoTelefone() {
        return tipoTelefone;
    }

    public void setTipoTelefone(String tipoTelefone) {
        this.tipoTelefone = tipoTelefone;
    }
}
