package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Vencimento_secretario {
    private int codVencimento;
    private String descricaoVencimento;
    private Double valorVencimento;
    private Double salarioLiquido;
    private int idSecretario;

    public int getCodVencimento() {
        return codVencimento;
    }

    public void setCodVencimento(int codVencimento) {
        this.codVencimento = codVencimento;
    }

    public String getDescricaoVencimento() {
        return descricaoVencimento;
    }

    public void setDescricaoVencimento(String descricaoVencimento) {
        this.descricaoVencimento = descricaoVencimento;
    }

    public Double getValorVencimento() {
        return valorVencimento;
    }

    public void setValorVencimento(Double valorVencimento) {
        this.valorVencimento = valorVencimento;
    }

    public Double getSalarioLiquido() {
        return salarioLiquido;
    }

    public void setSalarioLiquido(Double salarioLiquido) {
        this.salarioLiquido = salarioLiquido;
    }

    public int getIdSecretario() {
        return idSecretario;
    }

    public void setIdSecretario(int idSecretario) {
        this.idSecretario = idSecretario;
    }
}
