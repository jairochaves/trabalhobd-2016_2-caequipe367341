package jairo.bancodedados.bd.beans;

import java.sql.Date;

/**
 *
 * @author jairochaves
 */
public class Staff {
    private int idStaff;
    private String nomeStaff;
    private String sexo;                  //M ou F
    private Date dataNascStaff;
    private String regimeDeTrabalho;    //'celetista','terceirizado'
    private String login;
    private String senha;
    private String jornadaDeTrabalho;
    private int idSupervisor;
    private int departamento;
    private String funcao;

    public int getIdStaff() {
        return idStaff;
    }

    public void setIdStaff(int idStaff) {
        this.idStaff = idStaff;
    }

    public String getNomeStaff() {
        return nomeStaff;
    }

    public void setNomeStaff(String nomeStaff) {
        this.nomeStaff = nomeStaff;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Date getDataNascStaff() {
        return dataNascStaff;
    }

    public void setDataNascStaff(Date dataNascStaff) {
        this.dataNascStaff = dataNascStaff;
    }

    public String getRegimeDeTrabalho() {
        return regimeDeTrabalho;
    }

    public void setRegimeDeTrabalho(String regimeDeTrabalho) {
        this.regimeDeTrabalho = regimeDeTrabalho;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getJornadaDeTrabalho() {
        return jornadaDeTrabalho;
    }

    public void setJornadaDeTrabalho(String jornadaDeTrabalho) {
        this.jornadaDeTrabalho = jornadaDeTrabalho;
    }

    public int getIdSupervisor() {
        return idSupervisor;
    }

    public void setIdSupervisor(int idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public int getDepartamento() {
        return departamento;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
}
