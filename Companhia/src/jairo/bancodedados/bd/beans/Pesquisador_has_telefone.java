package jairo.bancodedados.bd.beans;

/**
 *
 * @author jairochaves
 */
public class Pesquisador_has_telefone {
    private int codPesquisador;
    private int codTelefone;

    public int getCodPesquisador() {
        return codPesquisador;
    }

    public void setCodPesquisador(int codPesquisador) {
        this.codPesquisador = codPesquisador;
    }

    public int getCodTelefone() {
        return codTelefone;
    }

    public void setCodTelefone(int codTelefone) {
        this.codTelefone = codTelefone;
    }
}
