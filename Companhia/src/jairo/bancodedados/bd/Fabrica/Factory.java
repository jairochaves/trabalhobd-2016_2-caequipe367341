package jairo.bancodedados.bd.Fabrica;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author jairochaves
 */
public class Factory {
  public final String  URL="jdbc:mysql://localhost/caequipe367341";
  
    public Connection getconection(){
        String caminho="";
        String user="";
        String senha="";
        //seleciona o sistema operacional
        
        String sisOper=System.getProperty("os.name");
        if (sisOper.equals("Linux")) {
            caminho="/tmp/login.tmp";
        } else if(sisOper.equals("Windows")) {
            caminho="c://Windows/Temp/login.tmp";
        } else{
            JOptionPane.showMessageDialog(null, "O programa não funciona nesse S.O");
        }
        
        try(BufferedReader br=new BufferedReader(new FileReader(caminho))){
            user=br.readLine();
            senha=br.readLine();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        
        try {
            return DriverManager.getConnection(URL,user,senha);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException(e);
        }
        
    }
    
}
