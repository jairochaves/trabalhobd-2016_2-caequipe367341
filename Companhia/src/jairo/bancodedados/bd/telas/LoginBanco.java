package jairo.bancodedados.bd.telas;

import jairo.bancodedados.bd.Fabrica.Factory;
import jairo.bancodedados.bd.Menu_Global;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jairochaves
 */
public class LoginBanco extends javax.swing.JFrame {

    /**
     * Creates new form LoginBanco
     */
    public LoginBanco() {
        initComponents();
        this.setSize(400, 200);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        //this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        mensagemLabel = new javax.swing.JLabel();
        mensagem2Label = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        userTextField = new javax.swing.JTextField();
        senhabdPasswordField = new javax.swing.JPasswordField();
        cancelar = new javax.swing.JButton();
        userLabel = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setLayout(null);

        mensagemLabel.setText("Antes de começar, forneça o usuário e senha ");
        jPanel1.add(mensagemLabel);
        mensagemLabel.setBounds(50, 10, 330, 17);

        mensagem2Label.setText("do banco de dados");
        jPanel1.add(mensagem2Label);
        mensagem2Label.setBounds(140, 30, 140, 17);

        passwordLabel.setText("Senha:");
        jPanel1.add(passwordLabel);
        passwordLabel.setBounds(40, 110, 50, 17);
        jPanel1.add(userTextField);
        userTextField.setBounds(110, 60, 230, 27);
        jPanel1.add(senhabdPasswordField);
        senhabdPasswordField.setBounds(110, 110, 230, 27);

        cancelar.setText("Cancelar");
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });
        jPanel1.add(cancelar);
        cancelar.setBounds(240, 160, 90, 29);

        userLabel.setText("Usuário:");
        jPanel1.add(userLabel);
        userLabel.setBounds(40, 60, 70, 17);

        jButton1.setText("Iniciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(80, 150, 150, 40);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //Pega os valores digitados nos campos de texto e de senha
        String user=userTextField.getText();
        String senha=new String(senhabdPasswordField.getPassword());
        
        //Verifica se os valores inseridos sao válidos 
        if((user.equals(""))||(senha.equals(""))||(user==null)||(senha==null)){
            JOptionPane.showMessageDialog(null, "Verifique os valores inseridos");
        }else{

            String caminho="";
            //seleciona o sistema operacional

            String sisOper=System.getProperty("os.name");
            if (sisOper.equals("Linux")) {
                caminho="/tmp/login.tmp";
            } else if(sisOper.equals("Windows")) {
                caminho="c://Windows/Temp/login.tmp";
            } else{
                JOptionPane.showMessageDialog(null, "O programa não funciona nesse S.O");
            }
            //cria um arquivo temporario para armazenar o usuario e senha
            try (BufferedWriter bw=new BufferedWriter(new FileWriter(caminho))){
                    bw.write(user);
                    bw.newLine();
                    bw.write(senha);

                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            
            //Verifica se o usuário e senha estão corretos
            try {
                Connection fac=new Factory().getconection();
                System.out.println("Conectou");
                
                LoginUser login=new LoginUser();
                login.setVisible(true);
                this.dispose();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Usuário ou senha incorretos");
                userTextField.setText("");
                senhabdPasswordField.setText("");
            }
            
        }
        

    }//GEN-LAST:event_jButton1ActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginBanco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginBanco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginBanco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginBanco.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginBanco().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelar;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel mensagem2Label;
    private javax.swing.JLabel mensagemLabel;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JPasswordField senhabdPasswordField;
    private javax.swing.JLabel userLabel;
    private javax.swing.JTextField userTextField;
    // End of variables declaration//GEN-END:variables
}
